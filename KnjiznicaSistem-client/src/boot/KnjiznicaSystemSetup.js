// import Vue from 'vue'

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

const firebaseConfig = {
  apiKey: 'AIzaSyDQ-sBIR_qx_Do6NQVZo2ncZDVAGjxg5ic',
  authDomain: 'knjiznicasistem.firebaseapp.com',
  projectId: 'knjiznicasistem',
  storageBucket: 'knjiznicasistem.appspot.com',
  messagingSenderId: '33914860861',
  appId: '1:33914860861:web:5a962cc1f72d4b8ed64154',
  measurementId: 'G-Z31914B3V0'
}

firebase.initializeApp(firebaseConfig)

export default ({ Vue }) => {
  Vue.prototype.$auth = firebase.auth()
  // Vue.protoype.$db = firebase.firestore()
  // Vue.protoype.$storage = firebase.storage()
}

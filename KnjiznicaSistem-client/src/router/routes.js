const routes = [
  {
    path: '/',
    component: () => import('layouts/WelcomeLayout.vue'),
    children: [
      { path: '', component: () => import('pages/KnjiznicaSystem/WelcomeIndex.vue') }
    ]
  },

  {
    path: '/Login',
    component: () => import('layouts/LoginPageLayout.vue'),
    children: [
      {
        path: '',
        component: () =>
          import('pages/Login/LoginIndex.vue')
      }
    ]
  },
  {
    path: '/Administration',
    component: () => import('layouts/KnjiznicaSystemLayout.vue'),
    meta: { auth: true },
    children: [
      {
        path: '/Knjige',
        meta: { auth: true },
        component: () =>
          import('pages/KnjiznicaSystem/KnjigeIndex.vue')
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes

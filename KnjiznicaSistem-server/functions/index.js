const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var admin = require("firebase-admin");

var serviceAccount = require("./knjiznicasistem-firebase-adminsdk-se7zt-544765e3e6");
const { response } = require('express');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://knjiznicasistem-se7zt.firebaseio.com"
});

const db = admin.firestore();

app.get('/hello', (request, response) => {
 return response.send('Hello world');
});

app.post('/testPost', (request, response) => {
    const data = request.body;
    console.log(data.testdata);
    return response.send('POST metoda -> Add '+data.testdata);
});

app.put('/testPut', (request, response) => {
    const data = request.body;
    console.log(data.testdata);
    return response.send('PUT metoda -> Change '+data.testdata);
})

app.delete('/testDelete', (request, response) => {
    const data = request.body;
    console.log('Delete '+data.testdata);
    return response.send('Delete '+data.testdata);
})
   

//get funkcija po nekom kriteriju
db.collection("knjige").where("autor", "==", "Brett Easton Ellis")
    .get()
    .then(function(querySnapshot){
        querySnapshot.forEach(function(doc) {
            console.log(doc.id, " => ", doc.data());
        });
    })
    .catch(function(error) {
        console.log("Error getting documents", error);
    });

//ispisi sve knjige
app.get('/inventarK', (request, response) => {
    let res = [];
    db.collection('knjige')
    .get()
    .then((querySnapshot) => {
    querySnapshot.forEach((doc) => {
    let document = {
    id: doc.id,
    data: doc.data()
    }
    res.push(document)
    })
    return response.send(res)
    })
    .catch ((error) => {
    return response.send("Error getting documents: ", error);
    })
   })
   
//dodaj knjigu u bazu podataka
app.post('/inventarK', (request, response) => {
    if (Object.keys(request.body).length) {
        db.collection('knjige').doc().set(request.body)
            .then(function() {
                return response.send("Document successfully written - created!")
            })
            .catch (function (error) {
                return response.send("Error writing document: "+error)

            })
    } else {
        return response.send("No post data for new document. "+"A new document is not created")
    }
})

//editaj knjigu na bazi
app.put ('/inventarK', (request, response) => {
    if (Object.keys(request.body).length) {
        if (typeof request.query.id !== 'undefined') {
            db.collection('knjige')
                .doc (request.query.id)
                .set (request.body)
                .then (function () {
                    return response.send("Document successfully written - updated!")
                })
                .catch (function (error) {
                    return response.send("Error writing document: " + error)
                })
        } else {
            return response.send("A parameter is not set, a document is not updated!")
        }
    } else {return response.send("No post data for new document. A document is not updated!")}
})

//izbrisi nesto sa invetara
app.delete ('/inventarK', (request, response) => {
    if (typeof request.query.id !== 'undefined') {
        db.collection('knjige').doc(request.query.id).delete()
            .then(function () {
                return response.send(
                    "Document successfully deleted!"
                )
            })
            .catch(function (error) {
                return response.send(
                    "Error removing document: "+error
                )
            })
    } else {
        return response.send(
            "A parameter is not set. A document is not deleted"
        )
    }
})


//dolje su fje za rad s casopisima

//ispisi sve knjige
app.get('/inventarC', (request, response) => {
    let res = [];
    db.collection('casopisi')
    .get()
    .then((querySnapshot) => {
    querySnapshot.forEach((doc) => {
    let document = {
    id: doc.id,
    data: doc.data()
    }
    res.push(document)
    })
    return response.send(res)
    })
    .catch ((error) => {
    return response.send("Error getting documents: ", error);
    })
   })
   
//dodaj knjigu u bazu podataka
app.post('/inventarC', (request, response) => {
    if (Object.keys(request.body).length) {
        db.collection('casopisi').doc().set(request.body)
            .then(function() {
                return response.send("Document successfully written - created!")
            })
            .catch (function (error) {
                return response.send("Error writing document: "+error)

            })
    } else {
        return response.send("No post data for new document. "+"A new document is not created")
    }
})

//editaj knjigu na bazi
app.put ('/inventarC', (request, response) => {
    if (Object.keys(request.body).length) {
        if (typeof request.query.id !== 'undefined') {
            db.collection('casopisi')
                .doc (request.query.id)
                .set (request.body)
                .then (function () {
                    return response.send("Document successfully written - updated!")
                })
                .catch (function (error) {
                    return response.send("Error writing document: " + error)
                })
        } else {
            return response.send("A parameter is not set, a document is not updated!")
        }
    } else {return response.send("No post data for new document. A document is not updated!")}
})

//izbrisi nesto sa invetara
app.delete ('/inventarC', (request, response) => {
    if (typeof request.query.id !== 'undefined') {
        db.collection('casopisi').doc(request.query.id).delete()
            .then(function () {
                return response.send(
                    "Document successfully deleted!"
                )
            })
            .catch(function (error) {
                return response.send(
                    "Error removing document: "+error
                )
            })
    } else {
        return response.send(
            "A parameter is not set. A document is not deleted"
        )
    }
})




app.listen(3000, () => {
 console.log("Server running on port 3000");
});